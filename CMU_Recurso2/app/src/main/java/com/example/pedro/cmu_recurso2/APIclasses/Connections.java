package com.example.pedro.cmu_recurso2.APIclasses;

public class Connections {
    private String PowerKW;

    private String ID;

    private String ConnectionTypeID;

    public String getPowerKW ()
    {
        return PowerKW;
    }

    public void setPowerKW (String PowerKW)
    {
        this.PowerKW = PowerKW;
    }

    public String getID ()
    {
        return ID;
    }

    public void setID (String ID)
    {
        this.ID = ID;
    }

    public String getConnectionTypeID ()
    {
        return ConnectionTypeID;
    }

    public void setConnectionTypeID (String ConnectionTypeID)
    {
        this.ConnectionTypeID = ConnectionTypeID;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [PowerKW = "+PowerKW+", ID = "+ID+", ConnectionTypeID = "+ConnectionTypeID+"]";
    }
}
