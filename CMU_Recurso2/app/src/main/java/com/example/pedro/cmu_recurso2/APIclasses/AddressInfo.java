package com.example.pedro.cmu_recurso2.APIclasses;

public class AddressInfo {
    private String CountryID;

    private String ContactTelephone1;

    private String StateOrProvince;

    private String Title;

    private String Latitude;

    private String Postcode;

    private String Longitude;

    private String AddressLine2;

    private String AddressLine1;

    private String Town;

    private String RelatedURL;

    private String ContactEmail;

    private String ID;

    private String DistanceUnit;

    public String getCountryID ()
    {
        return CountryID;
    }

    public void setCountryID (String CountryID)
    {
        this.CountryID = CountryID;
    }

    public String getContactTelephone1 ()
    {
        return ContactTelephone1;
    }

    public void setContactTelephone1 (String ContactTelephone1)
    {
        this.ContactTelephone1 = ContactTelephone1;
    }

    public String getStateOrProvince ()
    {
        return StateOrProvince;
    }

    public void setStateOrProvince (String StateOrProvince)
    {
        this.StateOrProvince = StateOrProvince;
    }

    public String getTitle ()
    {
        return Title;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }

    public String getLatitude ()
    {
        return Latitude;
    }

    public void setLatitude (String Latitude)
    {
        this.Latitude = Latitude;
    }

    public String getPostcode ()
    {
        return Postcode;
    }

    public void setPostcode (String Postcode)
    {
        this.Postcode = Postcode;
    }

    public String getLongitude ()
    {
        return Longitude;
    }

    public void setLongitude (String Longitude)
    {
        this.Longitude = Longitude;
    }

    public String getAddressLine2 ()
    {
        return AddressLine2;
    }

    public void setAddressLine2 (String AddressLine2)
    {
        this.AddressLine2 = AddressLine2;
    }

    public String getAddressLine1 ()
    {
        return AddressLine1;
    }

    public void setAddressLine1 (String AddressLine1)
    {
        this.AddressLine1 = AddressLine1;
    }

    public String getTown ()
    {
        return Town;
    }

    public void setTown (String Town)
    {
        this.Town = Town;
    }

    public String getRelatedURL ()
    {
        return RelatedURL;
    }

    public void setRelatedURL (String RelatedURL)
    {
        this.RelatedURL = RelatedURL;
    }

    public String getContactEmail ()
    {
        return ContactEmail;
    }

    public void setContactEmail (String ContactEmail)
    {
        this.ContactEmail = ContactEmail;
    }

    public String getID ()
    {
        return ID;
    }

    public void setID (String ID)
    {
        this.ID = ID;
    }

    public String getDistanceUnit ()
    {
        return DistanceUnit;
    }

    public void setDistanceUnit (String DistanceUnit)
    {
        this.DistanceUnit = DistanceUnit;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [CountryID = "+CountryID+", ContactTelephone1 = "+ContactTelephone1+", StateOrProvince = "+StateOrProvince+", Title = "+Title+", Latitude = "+Latitude+", Postcode = "+Postcode+", Longitude = "+Longitude+", AddressLine2 = "+AddressLine2+", AddressLine1 = "+AddressLine1+", Town = "+Town+", RelatedURL = "+RelatedURL+", ContactEmail = "+ContactEmail+", ID = "+ID+", DistanceUnit = "+DistanceUnit+"]";
    }
}
