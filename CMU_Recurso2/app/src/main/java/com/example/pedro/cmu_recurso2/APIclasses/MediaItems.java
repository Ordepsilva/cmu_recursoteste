package com.example.pedro.cmu_recurso2.APIclasses;

public class MediaItems {
    private String ItemThumbnailURL;

    private String IsVideo;

    private String IsFeaturedItem;

    private String Comment;

    private User User;

    private String IsEnabled;

    private String ItemURL;

    private String DateCreated;

    private String ID;

    private String ChargePointID;

    private String IsExternalResource;

    public String getItemThumbnailURL ()
    {
        return ItemThumbnailURL;
    }

    public void setItemThumbnailURL (String ItemThumbnailURL)
    {
        this.ItemThumbnailURL = ItemThumbnailURL;
    }

    public String getIsVideo ()
    {
        return IsVideo;
    }

    public void setIsVideo (String IsVideo)
    {
        this.IsVideo = IsVideo;
    }

    public String getIsFeaturedItem ()
    {
        return IsFeaturedItem;
    }

    public void setIsFeaturedItem (String IsFeaturedItem)
    {
        this.IsFeaturedItem = IsFeaturedItem;
    }

    public String getComment ()
    {
        return Comment;
    }

    public void setComment (String Comment)
    {
        this.Comment = Comment;
    }

    public User getUser ()
    {
        return User;
    }

    public void setUser (User User)
    {
        this.User = User;
    }

    public String getIsEnabled ()
    {
        return IsEnabled;
    }

    public void setIsEnabled (String IsEnabled)
    {
        this.IsEnabled = IsEnabled;
    }

    public String getItemURL ()
    {
        return ItemURL;
    }

    public void setItemURL (String ItemURL)
    {
        this.ItemURL = ItemURL;
    }

    public String getDateCreated ()
    {
        return DateCreated;
    }

    public void setDateCreated (String DateCreated)
    {
        this.DateCreated = DateCreated;
    }

    public String getID ()
    {
        return ID;
    }

    public void setID (String ID)
    {
        this.ID = ID;
    }

    public String getChargePointID ()
    {
        return ChargePointID;
    }

    public void setChargePointID (String ChargePointID)
    {
        this.ChargePointID = ChargePointID;
    }

    public String getIsExternalResource ()
    {
        return IsExternalResource;
    }

    public void setIsExternalResource (String IsExternalResource)
    {
        this.IsExternalResource = IsExternalResource;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ItemThumbnailURL = "+ItemThumbnailURL+", IsVideo = "+IsVideo+", IsFeaturedItem = "+IsFeaturedItem+", Comment = "+Comment+", User = "+User+", IsEnabled = "+IsEnabled+", ItemURL = "+ItemURL+", DateCreated = "+DateCreated+", ID = "+ID+", ChargePointID = "+ChargePointID+", IsExternalResource = "+IsExternalResource+"]";
    }
}
