package com.example.pedro.cmu_recurso2;

public class ApiUtils {
    public static String BASE_URL = "https://api.openchargemap.io/v3";

    public static openchargemapAPI getopencharge() {
        return RetrofitClient.getClient(BASE_URL).create(openchargemapAPI.class);
    }
}
