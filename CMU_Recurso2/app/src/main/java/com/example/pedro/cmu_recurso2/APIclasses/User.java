package com.example.pedro.cmu_recurso2.APIclasses;

public class User {
    private String Username;

    private String ReputationPoints;

    private String ID;

    private String ProfileImageURL;

    public String getUsername ()
    {
        return Username;
    }

    public void setUsername (String Username)
    {
        this.Username = Username;
    }

    public String getReputationPoints ()
    {
        return ReputationPoints;
    }

    public void setReputationPoints (String ReputationPoints)
    {
        this.ReputationPoints = ReputationPoints;
    }

    public String getID ()
    {
        return ID;
    }

    public void setID (String ID)
    {
        this.ID = ID;
    }

    public String getProfileImageURL ()
    {
        return ProfileImageURL;
    }

    public void setProfileImageURL (String ProfileImageURL)
    {
        this.ProfileImageURL = ProfileImageURL;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Username = "+Username+", ReputationPoints = "+ReputationPoints+", ID = "+ID+", ProfileImageURL = "+ProfileImageURL+"]";
    }
}
