package com.example.pedro.cmu_recurso2;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.example.pedro.cmu_recurso2.APIclasses.Local;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Local> list;

    public MyAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<>();
    }

    public MyAdapter(List<Local> listitems, Context context) {
        this.list = (ArrayList<Local>) listitems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        final Local localcharg = list.get(i);

        //for (int j = 0; j < 10; j++) {
        holder.textview1.setText(localcharg.getAddressInfo().getTitle());
        holder.textview2.setText(localcharg.getAddressInfo().getAddressLine1());
        holder.textview3.setText(localcharg.getAddressInfo().getTown());
        //}
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textview1, textview2, textview3, textview4;
        CardView cardView;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            textview1 = (TextView) itemView.findViewById(R.id.textviewnome);
            textview2 = (TextView) itemView.findViewById(R.id.textviewmorada);
            textview3 = (TextView) itemView.findViewById(R.id.textviewcity);

            cardView = itemView.findViewById(R.id.cardview);


        }
    }

    public void addItems(List<Local> listToAdd) {
        list.addAll(listToAdd);
        notifyDataSetChanged();
    }
}
