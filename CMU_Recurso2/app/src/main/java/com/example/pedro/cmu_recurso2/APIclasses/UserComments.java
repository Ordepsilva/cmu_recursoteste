package com.example.pedro.cmu_recurso2.APIclasses;

public class UserComments {
    private String UserName;

    private User User;

    private String RelatedURL;

    private String DateCreated;

    private String CheckinStatusTypeID;

    private String ID;

    private String CommentTypeID;

    private String ChargePointID;

    public String getUserName ()
    {
        return UserName;
    }

    public void setUserName (String UserName)
    {
        this.UserName = UserName;
    }

    public User getUser ()
    {
        return User;
    }

    public void setUser (User User)
    {
        this.User = User;
    }

    public String getRelatedURL ()
    {
        return RelatedURL;
    }

    public void setRelatedURL (String RelatedURL)
    {
        this.RelatedURL = RelatedURL;
    }

    public String getDateCreated ()
    {
        return DateCreated;
    }

    public void setDateCreated (String DateCreated)
    {
        this.DateCreated = DateCreated;
    }

    public String getCheckinStatusTypeID ()
    {
        return CheckinStatusTypeID;
    }

    public void setCheckinStatusTypeID (String CheckinStatusTypeID)
    {
        this.CheckinStatusTypeID = CheckinStatusTypeID;
    }

    public String getID ()
    {
        return ID;
    }

    public void setID (String ID)
    {
        this.ID = ID;
    }

    public String getCommentTypeID ()
    {
        return CommentTypeID;
    }

    public void setCommentTypeID (String CommentTypeID)
    {
        this.CommentTypeID = CommentTypeID;
    }

    public String getChargePointID ()
    {
        return ChargePointID;
    }

    public void setChargePointID (String ChargePointID)
    {
        this.ChargePointID = ChargePointID;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [UserName = "+UserName+", User = "+User+", RelatedURL = "+RelatedURL+", DateCreated = "+DateCreated+", CheckinStatusTypeID = "+CheckinStatusTypeID+", ID = "+ID+", CommentTypeID = "+CommentTypeID+", ChargePointID = "+ChargePointID+"]";
    }
}
