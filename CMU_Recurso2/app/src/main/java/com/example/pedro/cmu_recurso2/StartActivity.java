package com.example.pedro.cmu_recurso2;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class StartActivity extends AppCompatActivity implements View.OnClickListener {
    Button carregamentosprox, carregamentoseffec, meuscarros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        carregamentosprox = findViewById(R.id.carregamentosprox);
        carregamentoseffec = findViewById(R.id.carregamentoseffec);
        meuscarros = findViewById(R.id.meuscarros);


        carregamentoseffec.setOnClickListener(this);
        meuscarros.setOnClickListener(this);
        carregamentosprox.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.carregamentosprox) {
            startActivity(new Intent(this, MainActivity.class));
        } else if (v.getId() == R.id.carregamentoseffec) {

        } else if (v.getId() == R.id.meuscarros) {

        }
    }

}
