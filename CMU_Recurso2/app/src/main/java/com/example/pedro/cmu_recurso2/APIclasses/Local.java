package com.example.pedro.cmu_recurso2.APIclasses;

public class Local {
    private Connections[] Connections;

    private String NumberOfPoints;

    private UserComments[] UserComments;

    private MediaItems[] MediaItems;

    private String UsageTypeID;

    private String DateCreated;

    private String OperatorID;

    private String SubmissionStatusTypeID;

    private AddressInfo AddressInfo;

    private String DataQualityLevel;

    private String DataProviderID;

    private String IsRecentlyVerified;

    private String DateLastStatusUpdate;

    private String ID;

    private String UUID;

    private String StatusTypeID;

    private String DateLastVerified;

    public Connections[] getConnections ()
    {
        return Connections;
    }

    public void setConnections (Connections[] Connections)
    {
        this.Connections = Connections;
    }

    public String getNumberOfPoints ()
    {
        return NumberOfPoints;
    }

    public void setNumberOfPoints (String NumberOfPoints)
    {
        this.NumberOfPoints = NumberOfPoints;
    }

    public UserComments[] getUserComments ()
    {
        return UserComments;
    }

    public void setUserComments (UserComments[] UserComments)
    {
        this.UserComments = UserComments;
    }

    public MediaItems[] getMediaItems ()
    {
        return MediaItems;
    }

    public void setMediaItems (MediaItems[] MediaItems)
    {
        this.MediaItems = MediaItems;
    }

    public String getUsageTypeID ()
    {
        return UsageTypeID;
    }

    public void setUsageTypeID (String UsageTypeID)
    {
        this.UsageTypeID = UsageTypeID;
    }

    public String getDateCreated ()
    {
        return DateCreated;
    }

    public void setDateCreated (String DateCreated)
    {
        this.DateCreated = DateCreated;
    }

    public String getOperatorID ()
    {
        return OperatorID;
    }

    public void setOperatorID (String OperatorID)
    {
        this.OperatorID = OperatorID;
    }

    public String getSubmissionStatusTypeID ()
    {
        return SubmissionStatusTypeID;
    }

    public void setSubmissionStatusTypeID (String SubmissionStatusTypeID)
    {
        this.SubmissionStatusTypeID = SubmissionStatusTypeID;
    }

    public AddressInfo getAddressInfo ()
    {
        return AddressInfo;
    }

    public void setAddressInfo (AddressInfo AddressInfo)
    {
        this.AddressInfo = AddressInfo;
    }

    public String getDataQualityLevel ()
    {
        return DataQualityLevel;
    }

    public void setDataQualityLevel (String DataQualityLevel)
    {
        this.DataQualityLevel = DataQualityLevel;
    }

    public String getDataProviderID ()
    {
        return DataProviderID;
    }

    public void setDataProviderID (String DataProviderID)
    {
        this.DataProviderID = DataProviderID;
    }

    public String getIsRecentlyVerified ()
    {
        return IsRecentlyVerified;
    }

    public void setIsRecentlyVerified (String IsRecentlyVerified)
    {
        this.IsRecentlyVerified = IsRecentlyVerified;
    }

    public String getDateLastStatusUpdate ()
    {
        return DateLastStatusUpdate;
    }

    public void setDateLastStatusUpdate (String DateLastStatusUpdate)
    {
        this.DateLastStatusUpdate = DateLastStatusUpdate;
    }

    public String getID ()
    {
        return ID;
    }

    public void setID (String ID)
    {
        this.ID = ID;
    }

    public String getUUID ()
    {
        return UUID;
    }

    public void setUUID (String UUID)
    {
        this.UUID = UUID;
    }

    public String getStatusTypeID ()
    {
        return StatusTypeID;
    }

    public void setStatusTypeID (String StatusTypeID)
    {
        this.StatusTypeID = StatusTypeID;
    }

    public String getDateLastVerified ()
    {
        return DateLastVerified;
    }

    public void setDateLastVerified (String DateLastVerified)
    {
        this.DateLastVerified = DateLastVerified;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Connections = "+Connections+", NumberOfPoints = "+NumberOfPoints+", UserComments = "+UserComments+", MediaItems = "+MediaItems+", UsageTypeID = "+UsageTypeID+", DateCreated = "+DateCreated+", OperatorID = "+OperatorID+", SubmissionStatusTypeID = "+SubmissionStatusTypeID+", AddressInfo = "+AddressInfo+", DataQualityLevel = "+DataQualityLevel+", DataProviderID = "+DataProviderID+", IsRecentlyVerified = "+IsRecentlyVerified+", DateLastStatusUpdate = "+DateLastStatusUpdate+", ID = "+ID+", UUID = "+UUID+", StatusTypeID = "+StatusTypeID+", DateLastVerified = "+DateLastVerified+"]";
    }
}
