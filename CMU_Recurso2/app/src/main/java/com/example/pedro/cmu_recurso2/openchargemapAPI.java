package com.example.pedro.cmu_recurso2;

import com.example.pedro.cmu_recurso2.APIclasses.Local;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface openchargemapAPI {

    @GET("poi/")
    Call<List<Local>> getlocalcharge(@Query("output") String output,
                                     @Query("maxresults") String max,
                                     @Query("countrycode") String countrycode,
                                     @Query("latitude") String latitude,
                                     @Query("longitude") String longitude,
                                     @Query("distance") String distance,
                                     @Query("distanceunit") String distanceunit);
}
